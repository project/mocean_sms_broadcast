<?php

namespace Drupal\mocean_sms_broadcast;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

class Utility {

 /**
  * SMS API.
  */
  public function smsBroadcastSendMessage($recipient, $message) {

    $sms_broadcast_settings = \Drupal::config('mocean_sms_broadcast.settings');

    $url = 'https://rest.moceanapi.com/rest/1/sms';
    $fields = array(
      'mocean-api-key'=>$sms_broadcast_settings->get('api_key'),
      'mocean-api-secret'=>$sms_broadcast_settings->get('api_secret'),
      'mocean-from'=>$sms_broadcast_settings->get('message_from'),
      'mocean-to'=>$recipient,
      'mocean-text'=>$message,
      'mocean-resp-format'=>'json',
      'mocean-medium'=>'drupal_broadcast'
    );

    $fields_string = '';
    foreach($fields as $key=>$value) {
      $fields_string .= $key.'='.$value.'&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);

    curl_close($ch);

    $json = json_decode($result, true);

    //returns an array
    return $json;
  }

 /**
  * Query API check credits.
  */
  public function smsBroadcastGetCredit() {

    $sms_broadcast_settings = \Drupal::config('mocean_sms_broadcast.settings');

    $url = 'https://rest.moceanapi.com/rest/1/account/balance?';
    $fields = array(
      'mocean-api-key'=>$sms_broadcast_settings->get('api_key'),
      'mocean-api-secret'=>$sms_broadcast_settings->get('api_secret'),
      'mocean-resp-format'=>'json'
    );

    $fields_string = '';
    foreach($fields as $key=>$value) {
      $fields_string .= $key.'='.$value.'&';
    }
    rtrim($fields_string, '&');

    $url_final = $url.$fields_string;

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url_final);
    curl_setopt($ch,CURLOPT_HTTPGET, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);

    curl_close($ch);

    $json = json_decode($result, true);

   //returns an array
	return $json;
  }

 /**
  * Query API check pricing.
  */
  public function smsBroadcastGetPricing() {

	$sms_login_settings = \Drupal::config('mocean_sms_broadcast.settings');

	$url = 'https://rest.moceanapi.com/rest/2/account/pricing?';
	$fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-resp-format'=>'json',
	  'mocean-type'=>'verify'
	);

	$fields_string = '';
	foreach($fields as $key=>$value) {
	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$url_final = $url.$fields_string;

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url_final);
	curl_setopt($ch,CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

	$json = json_decode($result, true);

	//returns an array
	return $json;
  }

 /**
  * Fetch field name for telephone.
  */
  public function getFieldName() {
    $sms_broadcast_settings = \Drupal::config('mocean_sms_broadcast.settings');
	  return $sms_broadcast_settings->get('field_name');
  }

  public static function getUserConsent()
  {
    return \Drupal::config('mocean_sms_broadcast.settings')->get("consent_form");
  }

}
