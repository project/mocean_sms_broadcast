<?php

namespace Drupal\mocean_sms_broadcast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\mocean_sms_broadcast\Utility;

/**
 * Provides a form for MoceanSMS settings.
 */
class SmsBroadcastSettingsForm extends ConfigFormBase {

  use MessengerTrait;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a SmsBroadcastSettingsFormSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder, RequestContext $request_context) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sms_broadcast_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $sms_broadcast_settings = $this->config('mocean_sms_broadcast.settings');

    $form['sms_broadcast_settings_form'] = [
      '#type' => 'details',
      '#title' => $this->t('MoceanSMS Settings'),
      '#open' => TRUE,
    ];

    $jsonResponse1 = (new Utility)->smsBroadcastGetCredit();
    $jsonResponse2 = (new Utility)->smsBroadcastGetPricing();

    if ($jsonResponse1['status'] == 0 && $jsonResponse2['status'] == 0)
	  $str = $jsonResponse2['destinations'][0]['currency'].' '.$jsonResponse1['value'];
    else {
      if ($jsonResponse1['status'] != 0)
        $str = $jsonResponse1['err_msg'];
      else if ($jsonResponse2['status'] != 0)
        $str = $jsonResponse2['err_msg'];
    }

    $form['sms_broadcast_settings_form']['credit_balance'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Credit Balance: '.$str),
    ];

    $form['sms_broadcast_settings_form']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('API key and secret can be found <a href="https://dashboard.moceanapi.com/">here</a>.'),
    ];

    $form['sms_broadcast_settings_form']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Your MoceanSMS API key.'),
      '#default_value' => $sms_broadcast_settings->get('api_key'),
      '#required' => TRUE,
    ];

    $form['sms_broadcast_settings_form']['api_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('API Secret'),
	    '#description' => $this->t('Your MoceanSMS API secret.'),
      '#default_value' => $sms_broadcast_settings->get('api_secret'),
      '#required' => TRUE,
    ];

	$form['sms_broadcast_settings_form']['message_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message From'),
      '#description' => $this->t('Sender of the SMS when the message is received at a mobile phone.'),
      '#default_value' => $sms_broadcast_settings->get('message_from'),
      '#maxlength' => 18,
      '#required' => TRUE,
    ];

    $form['sms_broadcast_settings_form']['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine Name of Telephone Field'),
	    '#description' => $this->t('Check for Manage Fields in Account Settings.'),
      '#default_value' => $sms_broadcast_settings->get('field_name'),
      '#required' => TRUE,
    ];

    $form['sms_broadcast_settings_form']['feedback_form'] = [
      '#type' => 'item',
      '#title' => $this->t('Did we do well ? Share your feedback with us '). '<a href="https://docs.google.com/forms/d/e/1FAIpQLSd1m2bhA1x--1wjeVq-zDqDZvJ5OzgqNz3vO29g8LayAUDSAw/viewform?usp=pp_url&entry.1302697225=Drupal+MoceanSMS+Broadcast" target="_blank">here!</a>',
      // '#required' => TRUE,
    ];
    $form['sms_broadcast_settings_form']['new_user'] = [
      '#type' => 'html_tag',
      '#tag' => 'b',
      '#value' => $this->t('New user ? Sign up <a href="https://dashboard.moceanapi.com/register?fr=drupal_broadcast">here</a> and get 20 trial SMS credits.'),
    ];

    $form['#attached']['library'][] = 'mocean_sms_broadcast/mocean_sms_broadcast';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mocean_sms_broadcast.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_secret', $form_state->getValue('api_secret'))
      ->set('message_from', $form_state->getValue('message_from'))
      ->set('field_name', $form_state->getValue('field_name'))
      ->save();

    $this->messenger()->addMessage($this->t('Settings saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mocean_sms_broadcast.settings'];
  }

}
