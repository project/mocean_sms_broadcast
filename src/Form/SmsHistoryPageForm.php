<?php

namespace Drupal\mocean_sms_broadcast\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\mocean_sms_broadcast\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SmsHistoryPageForm extends FormBase {

  protected $session;
  protected $cacheTagInvalidator;

  public function __construct(SessionInterface $session, CacheTagsInvalidatorInterface $invalidator) {
    $this->session = $session;
    $this->cacheTagInvalidator = $invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_history_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $keyword = $this->session->get('session_history.search', '');

    if($keyword!=''){
      $query = \Drupal::database()->select('mocean_sms_history', 's');
      $orGroup = $query->orConditionGroup()
        ->condition('sender', '%'.$keyword.'%', 'LIKE')
        ->condition('datetime', '%'.$keyword.'%', 'LIKE')
        ->condition('message', '%'.$keyword.'%', 'LIKE')
        ->condition('recipient','%'. $keyword.'%', 'LIKE')
        ->condition('response', '%'.$keyword.'%', 'LIKE')
        ->condition('status', '%'.$keyword.'%', 'LIKE');

      $results = \Drupal::database()->select('mocean_sms_history', 's')
        ->condition($orGroup)
        ->fields('s', array('pid','sender','datetime','message','recipient','response','status'))
        ->execute()->fetchAllAssoc('pid');

      $emptyResult = 'No results found.';
    }
    else {
      $results = \Drupal::database()->select('mocean_sms_history', 's')
        ->fields('s', array('pid','sender','datetime','message','recipient','response','status'))
        ->execute()->fetchAllAssoc('pid');

      $emptyResult = 'No SMS transaction history.';
    }

    $rows = array();
    foreach ($results as $row => $content) {
      $rows[] = array(
        'data' => array($content->sender, $content->datetime, $content->message
        , $content->recipient, $content->response, $content->status));
    }

    $header = array('Sender', 'Date & Time', 'Message', 'Recipient', 'Response', 'Status');

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => array_reverse($rows),
      '#empty' => $this->t($emptyResult),
    ];

    $form['container'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('container-inline'),
      ),
      '#prefix' => '<div align="right">',
      '#suffix' => '</div>',
      '#weight' => -1,
    );

    $form['container']['search'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#placeholder' => 'Search',
      '#default_value' => $this->session->get('session_history.search', ''),
    ];

    $form['container']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['#attached']['library'][] = 'mocean_sms_broadcast/mocean_sms_broadcast';

    $this->session->remove('session_history.search');
    $this->invalidateCacheTag();

    return $form;
  }

  protected function setSessionValue($key, $value) {
    if (empty($value)) {
      // If the value is an empty string, remove the key from the session.
      $this->session->remove($key);
    }
    else {
      $this->session->set($key, $value);
    }
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSessionValue('session_history.search', $form_state->getValue('search'));
    $this->invalidateCacheTag();
  }

  public function clearSession(array &$form, FormStateInterface $form_state) {
    $this->session->remove('session_history.search');
    $this->invalidateCacheTag();
  }

  protected function invalidateCacheTag() {
    $this->cacheTagInvalidator->invalidateTags(['session_history:' . $this->session->getId()]);
  }
}
