<?php

namespace Drupal\mocean_sms_broadcast\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for mocean_sms_broadcast pages.
 */
class SmsBroadcastController extends ControllerBase {

  public function smsBroadcastPage() {
    $build = [];
    $build['mocean_sms_broadcast_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_broadcast\Form\SmsBroadcastPageForm');
    return $build;
  }

  public function smsHistoryPage() {
    $build = [];
    $build['mocean_sms_history_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_broadcast\Form\SmsHistoryPageForm');
    return $build;
  }

}
